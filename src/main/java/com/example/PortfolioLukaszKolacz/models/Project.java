package com.example.PortfolioLukaszKolacz.models;

import com.example.PortfolioLukaszKolacz.models.forms.AddProjectForm;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by Lukasz Kolacz on 10.06.2017.
 */

@Entity
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String description;
    private String name;
    private String client;
    private Date date;
    private String img;
    private String git;

    public Project(){
    }

    public Project(String description, String name, String client, Date date, String img, String git) {
        this.description = description;
        this.name = name;
        this.client = client;
        this.date = date;
        this.img = img;
        this.git = git;
    }

    public Project(AddProjectForm projectForm) {
        description = projectForm.getDescription();
        name = projectForm.getName();
        client = projectForm.getClient();
        date = projectForm.getDate();
        img = projectForm.getImg();
        git=projectForm.getGit();

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getGit() {
        return git;
    }

    public void setGit(String git) {
        this.git = git;
    }
}
