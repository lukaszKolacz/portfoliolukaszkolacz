package com.example.PortfolioLukaszKolacz.models.forms;

import org.hibernate.validator.constraints.NotEmpty;
import java.sql.Date;


/**
 * Created by Lukasz Kolacz on 16.06.2017.
 */

public class AddProjectForm {
    @NotEmpty(message = "Not empty")
    private String description;

    @NotEmpty(message = "Not empty")
    private String client;

    @NotEmpty(message = "Not empty")
    private String name;

//    @NotEmpty(message = "Not empty")
    private Date date;

    @NotEmpty(message = "Not empty")
    private String img;

    @NotEmpty(message = "Not empty")
    private String git;

    public AddProjectForm(){

    }

    private AddProjectForm(Builder builder){
        description = builder.description;
        client = builder.client;
        name = builder.name;
        date = builder.date;
        img = builder.img;
        git = builder.git;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getGit() {
        return git;
    }

    public void setGit(String git) {
        this.git = git;
    }

    public static class Builder{
        private String description;
        private String client;
        private String name;
        private Date date;
        private String img;
        private String git;


        public Builder(String description){
            this.description = description;
        }
        public Builder client(String client){
            this.client = client;
            return this;
        }
        public Builder name(String name){
            this.name = name;
            return this;
        }
        public Builder date(java.util.Date date){
            this.date = new java.sql.Date(date.getTime());
            return this;
        }
        public Builder img(String img){
            this.img = img;
            return this;
        }
        public Builder git(String img){
            this.git = git;
            return this;
        }

        public AddProjectForm build(){
            return new AddProjectForm(this);
        }

    }
}
