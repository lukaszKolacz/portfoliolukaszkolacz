package com.example.PortfolioLukaszKolacz.repositiories;

import com.example.PortfolioLukaszKolacz.models.Project;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Lukasz Kolacz on 10.06.2017.
 */

@Repository
public interface ProjectRepository extends CrudRepository<Project, Integer>{
    List<Project> findAll();
}
