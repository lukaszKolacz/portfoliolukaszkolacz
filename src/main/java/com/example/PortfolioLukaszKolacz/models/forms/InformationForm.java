package com.example.PortfolioLukaszKolacz.models.forms;

/**
 * Created by Lukasz Kolacz on 26.06.2017.
 */


public class InformationForm {

    private int id;
    private String name;
    private String description;

    public InformationForm(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
