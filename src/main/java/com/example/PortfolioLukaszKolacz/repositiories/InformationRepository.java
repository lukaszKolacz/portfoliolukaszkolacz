package com.example.PortfolioLukaszKolacz.repositiories;

import com.example.PortfolioLukaszKolacz.models.Information;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/**
 * Created by Lukasz Kolacz on 16.06.2017.
 */

@Repository
public interface InformationRepository extends CrudRepository<Information, Integer>{

}
