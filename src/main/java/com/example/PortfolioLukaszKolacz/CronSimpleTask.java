package com.example.PortfolioLukaszKolacz;

import com.example.PortfolioLukaszKolacz.repositiories.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by Lukasz Kolacz on 10.06.2017.
 */

@Component
public class CronSimpleTask {

    @Autowired
    ProjectRepository projectRepository;

//    @Scheduled(fixedRate = 1000)
//    public void printSomething(){
//        System.out.println("Helooo!");
//    }

    //    s m g d m
//    * - co kazda
//    / - co iles
    @Scheduled(cron = " 0  0 17-21 * * MON-FRI")
    public void printSomething() {
        System.out.println("Helooo!");
    }
}
