package com.example.PortfolioLukaszKolacz.controllers;

import com.example.PortfolioLukaszKolacz.repositiories.AddProjectRepository;
import com.example.PortfolioLukaszKolacz.models.Project;
import com.example.PortfolioLukaszKolacz.models.UserInfoBean;
import com.example.PortfolioLukaszKolacz.models.forms.AddProjectForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

/**
 * Created by Lukasz Kolacz on 13.06.2017.
 */
@Controller
public class SecureController {
    @Autowired
    UserInfoBean userInfo;
    @Autowired
    AddProjectRepository addProjectRepository;


    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/admin")
    public String adminPage(Model model) {
        model.addAttribute("projectObject", new AddProjectForm());
        return "adminView";
    }
    @PostMapping("/admin")
    public String AddProjectPost(@ModelAttribute("projectObject") @Valid AddProjectForm projectForm, BindingResult result) {
        if (result.hasErrors()) {
            return "adminView";
        }
        Project projectObject = new Project(projectForm);
        addProjectRepository.save(projectObject);
        return "result";
    }

    @GetMapping("/403")
    @ResponseBody
    public String error403() {
        return "<center>Nie masz dostępu do tej strony!</center>";
    }
}
